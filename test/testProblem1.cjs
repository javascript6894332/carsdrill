// Import the inventory and problem1 modules
const inventory = require("../inventory.cjs");
const problem1 = require("../problem1.cjs");

// initializing id
const id = 33;
// Use problem1 function to find information about car with ID
const result = problem1(inventory, id);

// Check if the result array is empty
if (result.length !== 0) {
    // If the array is empty, prnting required format
    console.log(result);
} else {
    // If the array is not empty, printing empty array
    console.log(result);
}
