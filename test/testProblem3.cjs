// Importing necessary modules
const inventory = require("../inventory.cjs");
const problem3 = require("../problem3.cjs");

// Calling the function to get the result
const result = problem3(inventory);

//logging the result
console.log(result);
