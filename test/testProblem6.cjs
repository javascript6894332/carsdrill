// Importing necessary modules
const inventory = require("../inventory.cjs");
const problem6 = require("../problem6.cjs");

// Calling the function to get the result
const result = problem6(inventory);

//logging the result
console.log(JSON.stringify(result));
