// Importing necessary modules
const inventory = require("../inventory.cjs");
const problem4 = require("../problem4.cjs");

// Calling the function to get the result
const result = problem4(inventory);

//logging the result
console.log(result);
