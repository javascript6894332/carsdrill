// Importing necessary modules
const inventory = require("../inventory.cjs");
const problem4 = require("../problem4.cjs");
const problem5 = require("../problem5.cjs");

// Calling the function to get the result
const carYears = problem4(inventory);
const result = problem5(inventory, carYears);

//logging the result
console.log(result);
