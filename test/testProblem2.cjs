// Importing necessary modules
const inventory = require("../inventory.cjs");
const problem2 = require("../problem2.cjs");

// Calling the function to get the result
const result = problem2(inventory);

//logging the result
console.log(result);
