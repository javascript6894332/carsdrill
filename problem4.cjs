function getCarYears(inventory) {
    let carYears = [];

    if (inventory && Array.isArray(inventory)) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i]['car_year']) {
                carYears.push(inventory[i]['car_year']);
            }
        }
        return carYears;
    } else {
        return [];
    }
}

module.exports = getCarYears;
