// Function to recall car details based on the provided ID
function recallCarDetails(inventory, id) {
    // checking if 
    if (inventory && Array.isArray(inventory) && typeof id == 'number'){
        // Loop through the inventory array to find a car with the specified ID
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index]['id'] === id) {
                // Return the car details if the ID matches
                return `Car ${inventory[index].id} is a ${inventory[index]['car_make']} ${inventory[index]['car_model']} ${inventory[index]['car_year']}`;
            }
            
        }
        
    }
    // Return an empty array if no car with the specified ID was found or,
    // inventory is not array or id is not number
    return []
}

// Export the recallCarDetails function to make it accessible from other modules
module.exports = recallCarDetails;
