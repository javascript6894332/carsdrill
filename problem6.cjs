function getBmwAndAudiCars(inventory) {
    var bmwAndAudi = [];

    if (inventory && Array.isArray(inventory)) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i]['car_make']) {
                if (inventory[i]['car_make'] === "Audi" || inventory[i]['car_make'] === "BMW") {
                    bmwAndAudi.push(inventory[i]);
                }
            }
        }
        return bmwAndAudi;
    } else {
        return [];
    }
}

module.exports = getBmwAndAudiCars;
