function sortedCarModels(inventory) {
    let carModels = [];

    if (inventory && Array.isArray(inventory)) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i]['car_model']) {
                carModels.push(inventory[i]['car_model']);
            }
        }
        carModels.sort();
        return carModels;
    } else {
        return [];
    }
}

module.exports = sortedCarModels;
