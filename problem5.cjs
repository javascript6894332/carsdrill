function getCarOlderThan2k(inventory, carYears) {
    let numberOfCars = 0;

    for (let i = 0; i < carYears.length; i++) {
        if (carYears[i] < 2000) {
            numberOfCars += 1;
        }
    }

    return numberOfCars;
}

module.exports = getCarOlderThan2k;
