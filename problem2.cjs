function lastCarDetails(inventory) {
    if ( Array.isArray(inventory) && inventory.length > 0 ) {
        // If the inventory is not empty, return the details of the last car.
        // getting the last car's details using the length property-1
        return `Last car is a ${inventory[inventory.length - 1].car_make} ${inventory[inventory.length - 1].car_model}`;
    }else{
        return []
    }
}

// Exporting the function to be used in other modules.
module.exports = lastCarDetails;
